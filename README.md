# [gfortran](https://pkgs.alpinelinux.org/packages?name=gfortran&arch=x86_64)

GNU Fortran Compiler https://gcc.gnu.org/fortran/

## Official documentation
* https://gcc.gnu.org/onlinedocs/gfortran

## Unofficial documentation
* [sockets programming gfortran](https://stackoverflow.com/questions/10305689/sockets-programming-gfortran)

## Developement tools
* https://rextester.com/l/fortran_online_compiler

## (micro)Python ctypes and uctypes
* http://docs.micropython.org/en/latest/library/uctypes.html does not contain LoadLibrary yet (micropython 1.10)

## Similar packages
### Alpine
* https://www.google.com/search?q=fortran+compiler+site:alpinelinux.org
### Other distribution
* https://gitlab.com/debian-packages-demo/gfortran
### Source packages
* https://www.google.com/search?q=Fortran+compiler+front-end+for+LLVM
#### f18
* https://github.com/flang-compiler/f18
* [*NVIDIA Has Been Working On A New Fortran "f18" Compiler It Wants To Contribute To LLVM*](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-F18-Fortran-Compiler)
#### Flang
* Since Debian 10 (2019) buster https://packages.debian.org/en/buster/flang-7
  * Check if virtual package flang exists...
* https://github.com/flang-compiler/flang
* [*FLANG: NVIDIA Brings Fortran To LLVM*](https://www.phoronix.com/scan.php?page=news_item&px=LLVM-NVIDIA-Fortran-Flang)
* [*Building the LLVM Flang Fortran compiler*](https://www.scivision.co/flang-compiler-build-tips/)
